CPPFLAGS=-std=c++14 -Wall 

all:
	@g++ $(CPPFLAGS) -o fact.out factorial.cpp  
	

run:
	@fact.out 



clean:
	@printf "\033[31mRemoving objects (and temporary files)\033[0m\n"
	@rm -rf bin/*.o*